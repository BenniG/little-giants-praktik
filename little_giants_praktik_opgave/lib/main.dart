import 'package:flutter/material.dart';
import 'package:little_giants_praktik_opgave/homepage.dart';
import 'details.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/brewpage': (context) => Brewpage(),
        '/homepage': (context) => Homepage(),
      },
      initialRoute: '/homepage',
    );
  }
}
