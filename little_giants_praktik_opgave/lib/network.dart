import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:little_giants_praktik_opgave/brewmodel.dart';

class Network {
  Future<List<Brewery>> fetchBrewery(String breweryName) async {
    final response = await http.get(Uri.parse(
        'https://api.openbrewerydb.org/breweries/search?query=${breweryName}'));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      print(response.statusCode.toString());
      final parsed = jsonDecode(response.body).cast<Map<String, dynamic>>();
      final list =
          parsed.map<Brewery>((json) => Brewery.fromJson(json)).toList();
      return list;
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load brewery');
    }
  }
}
