import 'package:flutter/material.dart';
import 'package:little_giants_praktik_opgave/brewmodel.dart';

class Brewcard extends StatelessWidget {
  final Brewery brewery;

  const Brewcard({Key? key, required this.brewery}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        //Navigating to detailspage with brewery object
        Navigator.of(context).pushNamed('/brewpage', arguments: brewery);
      },
      child: Card(
        //Design of each brewery card for listView on homepage
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        margin: EdgeInsets.symmetric(vertical: 6.0, horizontal: 25.0),
        child: ListTile(
          title: Text(
            '${brewery.name}',
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.w700,
            ),
          ),
          subtitle: Text(brewery.breweryType),
        ),
      ),
    );
  }
}

class AppTitle extends StatelessWidget {
  const AppTitle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Title(
      //App title design
      color: Colors.black,
      child: Text(
        'Brewery App',
        style: TextStyle(
          fontSize: 30.0,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}
