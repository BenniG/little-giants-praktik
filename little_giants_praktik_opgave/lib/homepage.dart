import 'package:flutter/material.dart';
import 'package:little_giants_praktik_opgave/brewmodel.dart';
import 'components.dart';
import 'network.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  Network network = Network();
  late Future<List<Brewery>> brewerylist;
  List<Brewery> tempbrew = [];
  @override
  void initState() {
    super.initState();
  }

  String searchString = '';
  int indexEnd = 8;
  int indexPage = 0;
  int indexItem = 7;
  bool overLimit = true;
  @override
  Widget build(BuildContext context) {
    brewerylist = network.fetchBrewery(searchString);
    return FutureBuilder<List<Brewery>>(
      future: brewerylist,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          //Checking if the data exists, Setting indexpage and what is shown on screen
          indexItem = indexPage * 7;
          indexEnd = searchString == '' ? 1 : (indexPage + 1) * 7;
          overLimit = false;
          if (indexEnd > snapshot.data!.length) {
            //Checking if index overflows and handling overflows
            indexEnd = snapshot.data!.length;
            overLimit = true;
          }
          tempbrew = snapshot.data!.getRange(indexItem, indexEnd).toList();
          return Scaffold(
            backgroundColor: Colors.grey.shade200,
            body: SafeArea(
              child: Column(
                //HomePage Setup
                children: [
                  SizedBox(
                    height: 10.0,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 25.0,
                      ),
                      AppTitle(),
                    ],
                  ),
                  Card(
                    //SearchBar setup and searchString stateSetup
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0)),
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                    child: TextField(
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          suffixIcon: Icon(
                            Icons.search,
                            color: Colors.black,
                            size: 40.0,
                          ),
                          contentPadding: EdgeInsets.all(20),
                          hintText: 'Search Breweries...'),
                      onSubmitted: (value) {
                        setState(() {
                          indexPage = 0;
                          searchString = value;
                        });
                      },
                    ),
                  ),
                  Expanded(
                    //Building list with BrewCards
                    child: ListView.builder(
                        itemCount: tempbrew.length,
                        itemBuilder: (context, index) {
                          return Brewcard(
                            brewery: tempbrew[index],
                          );
                        }),
                  ),
                  BottomAppBar(
                    //TODOSetup widgets for pagecards
                    //Setting up bottombar with page navigation and pagi index
                    elevation: 0,
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        if (indexPage > 0)
                          IconButton(
                            //Setup for going back one page
                            onPressed: () {
                              setState(() {
                                overLimit = false;
                                indexPage = indexPage - 1;
                              });
                            },
                            icon: Icon(Icons.chevron_left),
                          ),
                        if (indexPage > 0)
                          Container(
                              //Setup for last page index
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey,
                                  width: 0.5,
                                ),
                                borderRadius: BorderRadius.circular(5.0),
                                color: Colors.white,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('${indexPage}'),
                              )),
                        SizedBox(
                          width: 10.0,
                        ),
                        Container(
                          //Setup for current page index
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.grey,
                              width: 0.5,
                            ),
                            borderRadius: BorderRadius.circular(5.0),
                            color: Colors.redAccent.shade100,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              '${indexPage + 1}',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        if (overLimit == false)
                          Container(
                            //Setup for next page index
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey,
                                width: 0.0,
                              ),
                              borderRadius: BorderRadius.circular(5.0),
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text('${indexPage + 2}'),
                            ),
                          ),
                        if (overLimit == false)
                          IconButton(
                            //Setup for going forward one page
                            onPressed: () {
                              setState(() {
                                indexPage = indexPage + 1;
                              });
                            },
                            icon: Icon(Icons.chevron_right),
                          ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
