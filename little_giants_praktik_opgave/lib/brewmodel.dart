import 'dart:convert';

Brewery breweryFromJson(String str) => Brewery.fromJson(json.decode(str));

//Setup for each brewery object, commented parts in case of future changes
class Brewery {
  Brewery({
    required this.id,
    required this.name,
    required this.breweryType,
    required this.street,
    required this.address2,
    required this.address3,
    required this.city,
    required this.countyProvince,
    required this.state,
    required this.postalCode,
    // required this.country,
    required this.longitude,
    required this.latitude,
    //required this.phone,
    // required this.updatedAt,
    // required this.createdAt,
  });

  int id;
  String name;
  String breweryType;
  String? street;
  dynamic address2;
  dynamic address3;
  String? city;
  dynamic countyProvince;
  String? state;
  String postalCode;
  //String country;
  String? longitude;
  String? latitude;
  //String phone;
  // DateTime updatedAt;
  // DateTime createdAt;

  factory Brewery.fromJson(Map<String, dynamic> json) => Brewery(
        //Creating brewery object
        id: json["id"],
        name: json["name"],
        breweryType: json["brewery_type"],
        street: json["street"],
        address2: json["address_2"],
        address3: json["address_3"],
        city: json["city"],
        countyProvince: json["county_province"],
        state: json["state"],
        postalCode: json["postal_code"],
        //country: json["country"],
        longitude: json["longitude"],
        latitude: json["latitude"],
        //phone: json["phone"],
        // websiteUrl: json["website_url"],
        // updatedAt: DateTime.parse(json["updated_at"]),
        // createdAt: DateTime.parse(json["created_at"]),
      );
}
