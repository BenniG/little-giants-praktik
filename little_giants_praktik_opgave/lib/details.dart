import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:little_giants_praktik_opgave/brewmodel.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart' as latLng;

class Brewpage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Setup navigation route and parsing latitude and longtitude strings to doubles
    final brewery = ModalRoute.of(context)?.settings.arguments as Brewery;
    double latiBrew =
        brewery.latitude == null ? 0.0 : double.parse('${brewery.latitude}');
    double longtiBrew =
        brewery.longitude == null ? 0.0 : double.parse('${brewery.longitude}');
    return Scaffold(
      body: SafeArea(
        child: Column(
          //Designing detailspage
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                //Setup of pop button
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.chevron_left_rounded,
                    size: 60.0,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 40.0,
            ),
            Row(
              children: [
                //Design brewery name and type
                SizedBox(
                  width: 35.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      brewery.name,
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      'Type: ${brewery.breweryType}',
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 30.0,
            ),
            Row(
              children: [
                //Setting up design of details and handeling null information
                SizedBox(
                  width: 35.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Adress:',
                      style: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(brewery.street == null
                        ? 'No street available.'
                        : '${brewery.street},'),
                    Text(brewery.city == null
                        ? 'No city available.'
                        : '${brewery.city},'),
                    Row(
                      children: [
                        Text(brewery.state == null
                            ? 'No state available.'
                            : '${brewery.state} '),
                        Text('${brewery.postalCode}'),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            if (latiBrew != 0.0)
              Expanded(
                child: FlutterMap(
                  //Setting up map and icons on the map
                  options: MapOptions(
                    center: latLng.LatLng(
                      latiBrew,
                      longtiBrew,
                    ),
                    boundsOptions:
                        FitBoundsOptions(padding: EdgeInsets.all(8.0)),
                    zoom: 13.0,
                  ),
                  layers: [
                    TileLayerOptions(
                        urlTemplate:
                            "https://api.mapbox.com/styles/v1/bennilgbx/ckqce8b49926y18nqxi7432zy/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiYmVubmlsZ2J4IiwiYSI6ImNrcWNlNmZxNTByaGkyb2sxazJsdmpxMHcifQ.vzCSQzRGNMFs53N6z_8wkQ",
                        subdomains: ['a', 'b', 'c']),
                    MarkerLayerOptions(
                      markers: [
                        Marker(
                          width: 45.0,
                          height: 45.0,
                          point: latLng.LatLng(latiBrew, longtiBrew),
                          builder: (context) => Container(
                            child: Icon(
                              Icons.location_on,
                              color: Colors.red,
                              size: 45.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            else
              Column(
                children: [
                  SizedBox(height: 200.0),
                  Center(child: Text('No map location available.'))
                ],
              ),
          ],
        ),
      ),
    );
  }
}
